# Docker Install


To start using Docker, you need to install it first.
This script will get you up to speed with docker, docker-compose and docker-cleanup in no time regardless of your OS.
To execute:


```shell
bash <(curl -s https://gitlab.com/syncit-group-open-source/devops/docker-setup/-/raw/master/docker.install)
```


!Mandatory! - Logout from the current session (exit SSH) for the docker privileges to be applied properly.
